# Módulo Drupal 8 countly

Módulo para integrar [Countly SDK Web](https://github.com/Countly/countly-sdk-web) nun sitio drupal.

## Configuración

Ir ao formulario de configuración `/admin/config/system/countly` para incluír `App Key` e `URL` de [Countly](https://count.ly/).  
